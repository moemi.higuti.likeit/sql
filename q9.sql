SELECT c.category_name, SUM(i.Item_price) AS total_price

 FROM like_it_question.item i,
      like_it_question.item_category c
      
WHERE i.category_id = c.category_id 
GROUP BY c.category_name ASC;
